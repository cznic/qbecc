# qbecc

Command qbecc is a C compiler.

# Installation

    $ go install modernc.org/qbecc@latest

# Documentation

See [pkg.go.dev/modernc.org/qbecc](https://pkg.go.dev/modernc.org/qbecc)
